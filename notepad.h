#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Notepad; }
QT_END_NAMESPACE

class Notepad : public QMainWindow
{
    Q_OBJECT

public:
    Notepad(QWidget *parent = nullptr);
    ~Notepad();

    void loadFile(const QString &fileName);

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_plainTextEdit_copyAvailable(bool b);

    void setCurrentFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    bool areYouSure();
    void writeSettings();
    void readSettings();

    void on_actionAbout_Qt_triggered();

    void on_action_About_triggered();

    void on_action_New_triggered();

    void on_action_Open_triggered();

    bool on_action_Save_triggered();

    bool on_actionSave_As_triggered();

private:
    Ui::Notepad *ui;
    QString curFile;
};
#endif // NOTEPAD_H
