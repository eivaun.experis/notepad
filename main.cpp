#include "notepad.h"

#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("eivaun");
    QCoreApplication::setApplicationName("Notepad");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::applicationName());
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(a);

    Notepad w;
    if(!parser.positionalArguments().isEmpty())
        w.loadFile(parser.positionalArguments().at(0));

    w.show();
    return a.exec();
}
