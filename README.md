# Notepad
A sample notepad app.

## Build
The project is built with Qt. Use Qt Creator to compile the application.

## Usage examples
Open new `notepad.exe` \
Open file `notepad.exe file.txt` 

## Contributors
Eivind Vold Aunebakk
